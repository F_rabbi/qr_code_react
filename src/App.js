import React, { useState } from 'react';
import QRCode from 'react-qr-code';

function App() {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [qrData, setQRData] = useState('');

  const handleGenerateQR = () => {
    const data = `${name},${email},${phone}`;
    setQRData(data);
  };

  return (
    <div className="App">
      <h1>QR Code Generator</h1>
      <div>
        <label>Name:</label>
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
      </div>
      <div>
        <label>Email:</label>
        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
      </div>
      <div>
        <label>Phone:</label>
        <input type="tel" value={phone} onChange={(e) => setPhone(e.target.value)} />
      </div>
      <button onClick={handleGenerateQR}>Generate QR Code</button>
      {qrData && <QRCode value={qrData} />}
    </div>
  );
}

export default App;
